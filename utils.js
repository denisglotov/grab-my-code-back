const puppeteer = require('puppeteer-core');

exports.sleep = async (usec) => new Promise(resolve => setTimeout(resolve, usec));

exports.connect = async (url) => {
  const browser = await puppeteer.connect({ browserURL: 'http://localhost:9222/', defaultViewport: null });
  console.log('Agent: ', await browser.userAgent());

  const page = await browser.newPage();
  await page.goto(url);

  console.log(`Opened ${url}...`);
  return [page, browser];
}

// Use from console:
//   utils = require('./utils')
//   utils.connect('http://quotes.toscrape.com/').then(res => {p = res[0]})
//   p.mainFrame()
