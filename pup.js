const fse = require('fs-extra');
const utils = require('./utils');


HOST = 'https://leetcode.com/submissions';


async function main() {
  const [page, browser] = await utils.connect(HOST);
  let tries = 5;
  while (true) {
    console.log(tries);
    const frame = await page.mainFrame();
    const subm = await frame.$$('#submission-list-app table tbody tr');
    const func = async sub => {
      const children = [];
      const listHandle = await sub.evaluateHandle(e => e.children);
      const props = await listHandle.getProperties();
      for (const prop of props.values()) {
        const element = prop.asElement();
        if (element) children.push(element);
      }
      const promises = children.map(e => e.evaluate(node => node.innerText));
      const link = await children[2].$('a');
      const href = await link.evaluate(e => e.href);
      promises.push(Promise.resolve(href));
      return Promise.all(promises);
    };
    console.log(await Promise.all(subm.map(func)));

    if (tries-- || !await frame.$('.next')) break;

    await page.click('.next');
    await utils.sleep(3000);
  }
  
  browser.disconnect();
};


main().then(() => console.log('Done!')).catch(err => console.error(err));
